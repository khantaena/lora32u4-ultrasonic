# คำอธิบาย

ใช้ LoRa32u4 วัดระยะห่างด้วย Ultrasonic
HC-SR04 ใช้ 5V(จากจีน) หลังใช้ 3.3V ได้ค่าไม่เสถียร

## HC-SR04

Follow picture

* Vcc --- 32u4(5V) Orange
* Trig --- 32u4(12) Green
* Echo --- 32u(11) Yellow
* Gnd --- 32u4(GND) White

![connect](images/connect.jpg)