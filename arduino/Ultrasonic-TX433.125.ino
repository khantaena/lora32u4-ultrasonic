// Feather9x_TX
// -*- mode: C++ -*-
// Example sketch showing how to create a simple messaging client (transmitter)
// with the RH_RF95 class. RH_RF95 class does not provide for addressing or
// reliability, so you should only use RH_RF95 if you do not need the higher
// level messaging abilities.
// It is designed to work with the other example Feather9x_RX

#include <SPI.h>
#include <RH_RF95.h>
#include <Ultrasonic.h>
#include "LowPower.h"

/* for feather32u4 */
#define RFM95_CS 8
#define RFM95_RST 4
#define RFM95_INT 7

#if defined(ESP8266)
  /* for ESP w/featherwing */ 
  #define RFM95_CS  2    // "E"
  #define RFM95_RST 16   // "D"
  #define RFM95_INT 15   // "B"

#elif defined(ESP32)  
  /* ESP32 feather w/wing */
  #define RFM95_RST     27   // "A"
  #define RFM95_CS      33   // "B"
  #define RFM95_INT     12   //  next to A

#elif defined(NRF52)  
  /* nRF52832 feather w/wing */
  #define RFM95_RST     7   // "A"
  #define RFM95_CS      11   // "B"
  #define RFM95_INT     31   // "C"
  
#elif defined(TEENSYDUINO)
  /* Teensy 3.x w/wing */
  #define RFM95_RST     9   // "A"
  #define RFM95_CS      10   // "B"
  #define RFM95_INT     4    // "C"
#endif


// Change to 434.0 or other frequency, must match RX's freq!
#define RF95_FREQ 433.125

#define DEBUG 0
// Singleton instance of the radio driver
RH_RF95 rf95(RFM95_CS, RFM95_INT);

Ultrasonic ultrasonic(12, 11);

void setup() 
{
  pinMode(RFM95_RST, OUTPUT);
  digitalWrite(RFM95_RST, HIGH);
  if(DEBUG){
   Serial.begin(9600);
   while (!Serial) {
    delay(1);
   }
  }
  delay(100);
  if(DEBUG){
   Serial.println("Feather LoRa TX Test!");
  }
  // manual reset
  digitalWrite(RFM95_RST, LOW);
  delay(10);
  digitalWrite(RFM95_RST, HIGH);
  delay(10);

  while (!rf95.init()) {
    if(DEBUG){
      Serial.println("LoRa radio init failed");
    }
    while (1);
  }
  if(DEBUG){
   Serial.println("LoRa radio init OK!");
  }
  
  RH_RF95::ModemConfig modem_config = {
    0x48, // Reg 0x1D: BW=125kHz, Coding=4/8, Header=explicit
    0x94, // Reg 0x1E: Spread=4096chips/symbol, CRC=enable
    0x00  // Reg 0x26: LowDataRate=On, Agc=On
  };
  rf95.setModemRegisters(&modem_config);
  if(DEBUG){
   Serial.print("Set Chirp parameter to: "); Serial.println("Bw31_25Cr48Sf512");
  }
  // Defaults after init are 434.0MHz, modulation GFSK_Rb250Fd250, +13dbM
  
  if (!rf95.setFrequency(RF95_FREQ)) {
    if(DEBUG){
     Serial.println("setFrequency failed");
    }
    while (1);
  }
  if(DEBUG){
    Serial.print("Set Freq to: "); Serial.println(RF95_FREQ);
  }
  // you can set transmitter powers from 5 to 23 dBm:
  rf95.setTxPower(10, false);
}

int16_t packetnum = 0;  // packet counter, we increment per xmission
int16_t dupcount = 0;  
uint8_t distValOld=0;

void loop()
{
  delay(1000);
  if(DEBUG){
   Serial.print("Distance in CM: ");
   Serial.println(ultrasonic.distanceRead());
  }

  uint8_t radiopacket[30];
  uint8_t distVal;
  
  distVal=ultrasonic.distanceRead();
  sprintf(radiopacket,"%d",distVal);

  delay(10);
  if (distVal != distValOld) {
   if(DEBUG){
    Serial.println("Transmitting..."); // Send a message to rf95_server
    Serial.print("Sending : "); Serial.println((char *)radiopacket);
   }
   rf95.send(radiopacket,sizeof(radiopacket)); 
   dupcount=0;
  }else if(dupcount<5){
    if(DEBUG){
     Serial.println("Transmitting DUP..."); // Send a message to rf95_server
     Serial.print("Sending DUP : "); Serial.println((char *)radiopacket);
    }
    rf95.send(radiopacket,sizeof(radiopacket)); 
    dupcount++;
  }else{
    if(dupcount == 35) {
      if(DEBUG){
       Serial.println("Transmitting DUP2..."); // Send a message to rf95_server
       Serial.print("Sending DUP2 : "); Serial.println((char *)radiopacket);
      }
      rf95.send(radiopacket,sizeof(radiopacket)); 
      dupcount=4;
    }
    dupcount++;
    if(DEBUG){
     Serial.println("Not changed, no send");
    }
  }

  if(DEBUG){
   Serial.println("Waiting for packet to complete..."); 
   delay(20);
   rf95.waitPacketSent();
   // Now wait for a reply
   uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];
   uint8_t len = sizeof(buf);
   Serial.println("Waiting for reply...");
   if (rf95.waitAvailableTimeout(2000))
   { 
    // Should be a reply message for us now   
    if (rf95.recv(buf, &len))
    {
      Serial.print("Got reply: ");
      Serial.println((char*)buf);
      Serial.print("RSSI: ");
      Serial.println(rf95.lastRssi(), DEC);    
     }
     else
     {
      Serial.println("Receive failed");
     }
   }
   else
   {
    Serial.println("No reply, is there a listener around?");
   }
  }
  distValOld=distVal;
  delay(1000); // Wait 5 second between transmits, could also 'sleep' here!
  LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);        
}